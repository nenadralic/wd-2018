var objPeople = [
	{
		username: 'nenad',
		password: '123',
		role: 'putnik'
	},
	{
		username: 'nemanja',
		password: '123',
		role: 'putnik'
	},
	{
		username: 'pera',
		password: '123',
		role: 'administrator'
	}
]
let objPeopleFix = JSON.stringify(objPeople);

localStorage.setItem("objPeople",objPeopleFix);

function login() {
	var username = $("input[name=username]").val().trim();
	var password = $("input[name=password]").val().trim();
	var br = 0;
	var korIme = "";
	let objPeopleGet = JSON.parse( localStorage.getItem("objPeople"));
	for(var i = 0; i < objPeopleGet.length; i++) {
		if(username == objPeopleGet[i].username && password == objPeopleGet[i].password && objPeopleGet[i].role == 'putnik') {
			br++;
			korIme = objPeopleGet[i].username;
			window.location.replace(
				"putnik.html?user=" + korIme 
			);
			break
		}
		if(username == objPeopleGet[i].username && password == objPeopleGet[i].password && objPeopleGet[i].role == 'administrator') {
			br++;
			korIme = objPeopleGet[i].username;
			window.location.replace(
				"administrator.html?user=" + korIme
			);
			break
		}
		 else {
			console.log('incorrect username or password')
		}
	}
	if(br==0) {
		alert('Neispravni login podatci!')
	}
}

function registerUser() {
	var registerUsername= $("input[name=newUsername]").val().trim();
	var registerPassword = $("input[name=newPassword]").val().trim();
	var br = 0;
	var newUser = {
		username: registerUsername,
		password: registerPassword,
		role: 'putnik'
	}
	if(newUser.username == '' || newUser.password == '')
		{
			alert('Morate popuniti sva polja!')
			br++;
			console.log(objPeopleFix)
		}
	for(var i = 0; i < objPeople.length; i++) {
		if(newUser.username == objPeople[i].username) {
			alert('Vec postoji korisnik sa tim korisnickim imenom!')
			br ++;
			break
		} 
		
	}
	if(br==0){
		objPeople.push(newUser)
		alert('Uspesno ste se registrovali!')
		window.location.replace(
			"prijava.html"
		);
	}
	
}
$(document).ready(function(e) {

	var name = getParamValue("user");
	$("#userSpan").text(name);
	$("#korImeSpan").text(name);

	var item = objPeople.find(item => item.username === name);
	var pass = item.password;
	var tip = item.role;

	$("#lozinkaSpan").text(pass);
	$("#roleSpan").text(tip);

	$("#korImeIzmena").val(name);
	$("#lozinkaIzmena").val(pass);
	$("#ulogaIzmena").val(tip);
});

function getParamValue(name) {
	var location = decodeURI(window.location.toString());
	var index = location.indexOf("?")+1;
	var subs = location.substring(index, location.length);
	var splitted = subs.split("&");

	for(i=0; i < splitted.length; i++) {
		var s = splitted[i].split("=");
		var pName  = s[0];
		var pValue = s[1];
		if(pName == name) {
			return pValue;
		}
	}	
}
function pregled(){
	var name = getParamValue("user");
	
	window.location.replace(
		"pregledKorisnik.html?user=" + name 
	);
}
function izmenaKor(){
	var name = getParamValue("user");
	
	window.location.replace(
		"izmenaKorisnika.html?user=" + name 
	);
}
 
function povratakPutnik(){
	var name = getParamValue("user");
	window.location.replace(
		"putnik.html?user=" + name
	);
}
function pregledAdmin()
{
	var name = getParamValue("user");
	
	window.location.replace(
		"pregledAdministrator.html?user=" + name 
	);
}

function kartaKorisnik(){
	var name = getParamValue("user");
	window.location.replace(
		"kartaKorisnik.html?user=" + name
	);
}
function btnPotvrdi(){
	var name = getParamValue("user");
	window.location.replace(
		"kartaKorisnik.html?user=" + name
	);
	alert('Uspesno ste kupili kartu!')
	
}
$(function () {
	$('[data-toggle="popover"]').popover()
  })
function povratakAdmin(){
	var name = getParamValue("user");
	window.location.replace(
		"administrator.html?user=" + name
	);
}
function kartaAdmin(){
	var name = getParamValue("user");
	window.location.replace(
		"kartaAdmin.html?user=" + name
	);
}
function brisanje(){
	alert('Uspesno obrisan korisnik')
	window.location.replace(
		"index.html" 
	);
}
function potvrdiIzmenu(){
	var name = getParamValue("user");
	alert('Uspesno izmenjen korisnik')
	window.location.replace(
		"putnik.html?user=" + name 
	);
}
function izmenaKor(){
	var name = getParamValue("user");
	window.location.replace(
		"izmenaKorisnika.html?user=" + name 
	);
}
function pregledAdminProfil(){
	var name = getParamValue("user");
	window.location.replace(
		"pregledAdminProfil.html?user=" + name 
	);
}
function potvrdiIzmenuAdmin(){
	var name = getParamValue("user");
	alert('Uspesno izmenjen korisnik')
	window.location.replace(
		"administrator.html?user=" + name 
	);
}
function izmenaAdmin(){
	var name = getParamValue("user");
	window.location.replace(
		"izmenaAdmin.html?user=" + name 
	);
}
function pregledSedista(){
	var name = getParamValue("user");
	window.location.replace(
		"sedistaAdim.html?user=" + name 
	);
}

